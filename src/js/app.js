// App object
App = {
  //object member variables
  web3Provider: null,//store web3 reference
  contracts: {},//instantiate an empty object
  account: null, //account of wallet
  /*
  init :  1.Setup web3 provider, 
          2.Load contracts,
          3,Binds events to html components
  */
  init: async () => {
    // Initialize web3 and set the provider to the testRPC.
    if (!typeof web3 !== 'undefined') {
      App.web3Provider = window.ethereum;
      web3 = new Web3(window.ethereum);
      web3.eth.getAccounts(function (err, accounts) {
        if (err != null) {
          $('#message').text(`Error : MetaMask not available`);
          $('#exampleModal').modal('show');
        }
        else if (accounts.length == 0){
          $('#message').text(`Error : User is not logged in to MetaMask`);
          $('#exampleModal').modal('show');
        } 
      });
    } else {
      // set the provider you want from Web3.providers
      App.web3Provider = new Web3.providers.HttpProvider('http://127.0.0.1:8545');
      web3 = new Web3(App.web3Provider);
    }
    //Next, we load the contracts
    await App.initContract();
    //Load account details
    await App.getAccountDetails();
    //Bind the DOM components
    await App.bindEvents();
  },
  /*
  initContract: Load the contract details
  */
  initContract: async () => {
    data = await $.getJSON('IteriaToken.json')
    // Get the necessary contract artifact file and instantiate it with truffle-contract.
    App.contracts.MyToken = TruffleContract(data);

    // Set the provider for our contract.
    App.contracts.MyToken.setProvider(App.web3Provider);
  },
  bindEvents: () => {
    $(document).on('click', '#transferButton', App.handleTransfer);
    $(document).on('click', '#mintButton', App.handleMint);
  },
  handleTransfer: (event) => {
    event.preventDefault();
    //Retrieve the values from the input box
    var amount = parseInt($('#MyTokenTransferAmount').val());
    var toAddress = $('#MyTokenTransferAddress').val();

    console.log('Transfer ' + amount + ' TT to ' + toAddress);

    //call the contract transfer method to execute the transfer
    App.tokenInstance.transfer(toAddress, amount, { from: App.account, gas: 100000 })
      .then(function (result) {
        $('#message').text(`Success : Transferred ${amount}`);
        $('#exampleModal').modal('show');
        return App.getBalances();
      }).catch(function (err) {
        console.log(err.message);
      });
  },
  handleMint: (event) => {
    event.preventDefault();
    //Retrieve the values from the input box
    var amount = parseInt($('#MyTokenMintAmount').val());

    console.log('Mint ' + amount + ' TT');

    //call the contract transfer method to execute the transfer
    App.tokenInstance.mint(App.account, amount, { from: App.account, gas: 100000 })
      .then(function (result) {
        $('#message').text(`Success : Minted ${amount}`);
        $('#exampleModal').modal('show');
        return App.getBalances();
      }).catch(function (err) {
        console.log(err.message);
      });
  },
  getAccountDetails: async () => {
    /*Retrieve all the accounts that is currently connected to the blockchain*/
    web3.eth.getAccounts(async (error, accounts) => {
      if (error) console.log(error);
      //Use the first account
      App.account = accounts[0];
      //Display the wallet address in the place holder
      $('#MyTokenWallet').text(App.account)
      //Test
      App.tokenInstance = await App.contracts.MyToken.deployed();
      result = await App.tokenInstance.symbol();
      $('#MyTokenSymbol').text(result);
      count = await App.tokenInstance.totalSupply()
      // Use our contract to retrieve account balance.
      await App.getBalances();
    })
  },
  getBalances: async () => {
    result = await App.tokenInstance.balanceOf(App.account);
    balance = result.toNumber();
    $('#MyTokenBalance').text(balance);

    count = await App.tokenInstance.totalSupply()
    result = "NOT OWNER"

    owner = await App.tokenInstance.owner();
    if (owner == App.account) {
      result = "IS OWNER"
      $('#mintToken').removeClass("d-none")
    }
    $('#MyTokenOwner').text(result);
  },
}
// Web page loaded event handler
$(() => {
  $(window).load(function () {
    App.init();
  });
});