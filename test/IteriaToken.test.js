const { expectEvent, expectRevert } = require('@openzeppelin/test-helpers');

const IteriaToken = artifacts.require('IteriaToken');

let iteriaToken;

contract('IteriaToken', function (accounts) {
  beforeEach(async function () {
    //accounts
    owner = accounts[0];
    user1 = accounts[1];
    user2 = accounts[2];
    other = accounts[3];
    iteriaToken = await IteriaToken.new({ from: owner});


  });

  describe('as ERC20Detailed', async function () {
    it('should have token name', async function () {
      let name = await iteriaToken.name();
      assert.equal(name, "IteriaToken");
    });

    it('should have token symbol', async function () {
      let symbol = await iteriaToken.symbol();
      assert.equal(symbol, "ITK");
    });

    it('should have token decimals', async function () {
      let decimals = await iteriaToken.decimals();
      assert.equal(decimals, 2);
    });
  });

  describe('as ERC20Mintable', async function () {
    it('owner should have minter role', async function () {
      let isMinter = await iteriaToken.owner();
      assert.equal(isMinter, owner);
    });

    it('minter should mint', async function () {
      assert(await iteriaToken.balanceOf(user1), 0);
      let isMinter = await iteriaToken.mint(user1, 100);
      assert(await iteriaToken.balanceOf(user1), 100);
    });
  });

  describe('as ERC20', async function () {
    beforeEach(async function () {
      iteriaToken = await IteriaToken.new();
      await iteriaToken.mint(user1, 1000);
    });

    it('should return totalSupply', async function () {
      assert(await iteriaToken.totalSupply(), 1000);
    });

    it('should return balanceOf', async function () {
      assert(await iteriaToken.balanceOf(owner), 0);
      assert(await iteriaToken.balanceOf(user1), 1000);
      assert(await iteriaToken.balanceOf(user2), 0);
      assert(await iteriaToken.balanceOf(other), 0);
    });

    it('should transfer', async function () {
      await iteriaToken.transfer(user2, 500, { from: user1 });
      assert(await iteriaToken.balanceOf(user1), 500);
      assert(await iteriaToken.balanceOf(user2), 500);
    });

    it('should fail transfer', async function () {
      await expectRevert.unspecified(
        iteriaToken.transfer(user2, 500, { from: other })
      );
      assert(await iteriaToken.balanceOf(user1), 1000);
      assert(await iteriaToken.balanceOf(user2), 0);
      assert(await iteriaToken.balanceOf(other), 0);
    });

    it('should approve', async function () {
      assert(await iteriaToken.allowance(user1, user2), 0);

      await iteriaToken.approve(user2, 500, { from: user1 });

      assert(await iteriaToken.balanceOf(user1), 1000);
      assert(await iteriaToken.balanceOf(user2), 0);
      assert(await iteriaToken.allowance(user1, user2), 500);
    });

    it('should return allowance', async function () {
      assert(await iteriaToken.allowance(user1, user2), 0);

      await iteriaToken.approve(user2, 1000, { from: user1 });

      assert(await iteriaToken.balanceOf(user1), 1000);
      assert(await iteriaToken.balanceOf(user2), 0);
      assert(await iteriaToken.allowance(user1, user2), 1000);
    });

    it('should transferFrom', async function () {
      assert(await iteriaToken.balanceOf(user1), 1000);
      assert(await iteriaToken.balanceOf(user2), 0);
      assert(await iteriaToken.balanceOf(other), 0);
      assert(await iteriaToken.allowance(user1, user2), 0);

      await iteriaToken.approve(user2, 500, { from: user1 });

      assert(await iteriaToken.balanceOf(user1), 1000);
      assert(await iteriaToken.balanceOf(user2), 0);
      assert(await iteriaToken.balanceOf(other), 0);
      assert(await iteriaToken.allowance(user1, user2), 500);

      await iteriaToken.transferFrom(user1, other, 250, { from: user2 });

      assert(await iteriaToken.balanceOf(user1), 750);
      assert(await iteriaToken.balanceOf(user2), 0);
      assert(await iteriaToken.balanceOf(other), 250);
      assert(await iteriaToken.allowance(user1, user2), 250);
    });

    it('should increaseAllowance', async function () {
      assert(await iteriaToken.balanceOf(user1), 1000);
      assert(await iteriaToken.balanceOf(user2), 0);
      assert(await iteriaToken.balanceOf(other), 0);
      assert(await iteriaToken.allowance(user1, user2), 0);

      await iteriaToken.approve(user2, 500, { from: user1 });

      assert(await iteriaToken.balanceOf(user1), 1000);
      assert(await iteriaToken.balanceOf(user2), 0);
      assert(await iteriaToken.balanceOf(other), 0);
      assert(await iteriaToken.allowance(user1, user2), 500);

      await iteriaToken.increaseAllowance(user2, 250, { from: user1 });

      assert(await iteriaToken.balanceOf(user1), 1000);
      assert(await iteriaToken.balanceOf(user2), 0);
      assert(await iteriaToken.balanceOf(other), 0);
      assert(await iteriaToken.allowance(user1, user2), 750);

    });

    it('should decreaseAllowance', async function () {
      assert(await iteriaToken.balanceOf(user1), 1000);
      assert(await iteriaToken.balanceOf(user2), 0);
      assert(await iteriaToken.balanceOf(other), 0);
      assert(await iteriaToken.allowance(user1, user2), 0);

      await iteriaToken.approve(user2, 500, { from: user1 });

      assert(await iteriaToken.balanceOf(user1), 1000);
      assert(await iteriaToken.balanceOf(user2), 0);
      assert(await iteriaToken.balanceOf(other), 0);
      assert(await iteriaToken.allowance(user1, user2), 500);

      await iteriaToken.decreaseAllowance(user2, 250, { from: user1 });

      assert(await iteriaToken.balanceOf(user1), 1000);
      assert(await iteriaToken.balanceOf(user2), 0);
      assert(await iteriaToken.balanceOf(other), 0);
      assert(await iteriaToken.allowance(user1, user2), 250);

    });

  });

  describe('as ERC20Pausable', async function () {
    beforeEach(async function () {
      iteriaToken = await IteriaToken.new();
      await iteriaToken.mint(user1, 1000);
    });

    it('should be unpaused by default', async function () {
      assert.equal(await iteriaToken.paused(), false);
    });

    it('should pause', async function () {
      assert.equal(await iteriaToken.paused(), false);
      await iteriaToken.pause();
      assert.equal(await iteriaToken.paused(), true);
    });

    it('should unpause', async function () {
      assert.equal(await iteriaToken.paused(), false);
      await iteriaToken.pause();
      assert.equal(await iteriaToken.paused(), true);
      await iteriaToken.unpause();
      assert.equal(await iteriaToken.paused(), false);
    });

    it('should not allow transfer when paused', async function () {
      assert.equal(await iteriaToken.paused(), false);
      await iteriaToken.pause();
      assert.equal(await iteriaToken.paused(), true);

      await expectRevert.unspecified(
        iteriaToken.transfer(user2, 100, { from: user1 })
      );

      assert(await iteriaToken.balanceOf(user1), 1000);
      assert(await iteriaToken.balanceOf(user2), 0);
    });

    it('should not allow transferFrom when paused', async function () {
      assert(await iteriaToken.allowance(user1, user2), 0);
      await iteriaToken.approve(user2, 500, { from: user1 });
      assert(await iteriaToken.allowance(user1, user2), 500);

      assert.equal(await iteriaToken.paused(), false);
      await iteriaToken.pause();
      assert.equal(await iteriaToken.paused(), true);

      await expectRevert.unspecified(
        iteriaToken.transferFrom(user1, user2, 100, { from: user2 })
      );

      assert(await iteriaToken.allowance(user1, user2), 500);
      assert(await iteriaToken.balanceOf(user1), 1000);
      assert(await iteriaToken.balanceOf(user2), 0);
    });

  });

  describe('as ERC20Burnable', async function () {
    beforeEach(async function () {
      iteriaToken = await IteriaToken.new({ from: owner});
      await iteriaToken.mint(user1, 1000);
    });

    it('should burn', async function () {
      assert.equal(await iteriaToken.balanceOf(user1), 1000);

      await iteriaToken.burn(100, { from: user1 });

      assert.equal(await iteriaToken.balanceOf(user1), 900);
    });

    it('should burnFrom', async function () {
      assert.equal(await iteriaToken.balanceOf(user1), 1000);
      assert.equal(await iteriaToken.balanceOf(user2), 0);
      assert.equal(await iteriaToken.allowance(user1, user2), 0);

      await iteriaToken.approve(user2, 200, { from: user1 });
      assert.equal(await iteriaToken.allowance(user1, user2), 200);
      await iteriaToken.burnFrom(user1, 100, { from: user2 });

      assert.equal(await iteriaToken.balanceOf(user1), 900);
      assert.equal(await iteriaToken.balanceOf(user2), 0);
      assert.equal(await iteriaToken.allowance(user1, user2), 100);
    });

  });

});
