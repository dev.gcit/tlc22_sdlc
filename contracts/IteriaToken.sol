// SPDX-License-Identifier: MIT
pragma solidity ^0.8.9;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";
import "@openzeppelin/contracts/security/Pausable.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract IteriaToken is ERC20, ERC20Burnable, Pausable, Ownable {
    string public _description = "Iteria Token";
    uint256 public INITIAL_SUPPLY = 10000000;

    constructor() ERC20("IteriaToken", "ITK") {
        _mint(msg.sender, INITIAL_SUPPLY * 10**decimals());
    }

    function decimals() public view virtual override returns (uint8) {
        return 2;
    }

    function description() public view virtual returns (string memory) {
        return _description;
    }

    function mint(address to, uint256 amount) public onlyOwner {
        _mint(to, amount);
    }

    function pause() public onlyOwner {
        _pause();
    }

    function unpause() public onlyOwner {
        _unpause();
    }

    function _beforeTokenTransfer(
        address from,
        address to,
        uint256 amount
    ) internal override whenNotPaused {
        super._beforeTokenTransfer(from, to, amount);
    }
}
